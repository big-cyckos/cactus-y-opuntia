extends RigidBody2D

var stopped_playing = false

func init(x,y,playerPos):
	position.x = x
	position.y = y
	if playerPos.x > x:
		linear_velocity.x = (abs(playerPos.x - position.x))/2
	else:
		linear_velocity.x = -((abs(playerPos.x - position.x))/2)
	linear_velocity.y = -100
	angular_velocity = rand_range(5,10)

func _on_Tequila_body_entered(body):
	for body in $Area2D.get_overlapping_bodies():
		if body.name == "Protag":
			body.call("attacked", 1)
	var explosion = preload("res://explosion.tscn").instance()
	get_parent().add_child(explosion)
	$AudioStreamPlayer.play()
	$CollisionShape2D.disabled = true # if not, it could collide with floor again and cause more explosions
	$Sprite.visible = false
	explosion.init(position.x,position.y - 8) #position.y - 8, cuz otherwise explosion takes place in floor and it looks bed
	
	
func _on_AudioStreamPlayer_finished():
	queue_free()
