extends KinematicBody2D

signal summon_meteors(summoner, amount)
signal start_falling()
signal emd

const _up = Vector2(0, -1)
var velocity = Vector2()
var health
var player
var is_dead = false
var stun_timer = 0
var targetX
export var stun_time = 0.5
export var max_health = 10
export var gravity = 10
export var speed = 100
export var maxGravity = 500
export var bulletVel = 200

func _ready():
	health = max_health
	targetX = get_position().x

func init(_player):
	player = _player
	$AnimatedSprite.play("Init")
	$Cooldown.start()

func _physics_process(delta): 
	# grawitacja
	if !is_on_floor():
		velocity.y += gravity
	velocity.y = min(velocity.y, maxGravity)
	velocity.x = 0

	#stun
	if stun_timer > 0:
		$AnimatedSprite.modulate = Color(1.8, 1.8, 1.8, fmod(stun_timer, 0.25))
		stun_timer -= delta
	else:
		$AnimatedSprite.modulate = Color(1, 1, 1, 1)

	# super zaawansowane AI
	if player != null && $Cooldown.is_stopped():
		$Cooldown.start()
		var act = randi() % 3
		# Atak odbywa się po zakończeniu animacji
		if act == 0:
			$AnimatedSprite.play("Shoot")
		elif act == 1:
			$AnimatedSprite.play("Summon")
			_summon_meteors()
		else:
			targetX = float(randi() % 240 + 40)

	#jak różnica między aktualną pozycją a docelową jest > 16 to idź, a jak nie to obróć się do gracza
	var posDiff = targetX - get_position().x
	if abs(posDiff) < 32:
		$AnimatedSprite.flip_h = (player.get_position().x < get_position().x)
	else:
		velocity.x = abs(speed) * (posDiff / abs(posDiff))
		$AnimatedSprite.flip_h = (velocity.x < 0)
	
	move_and_slide(velocity, _up)

func _attack_player():
	if !is_dead:
		$ShotSound.play()
		var bullet = preload("res://bossbullet.tscn").instance()
		bullet.add_collision_exception_with(self)
		get_parent().add_child(bullet)
		bullet.init(position.x,position.y,player.get_position(),bulletVel)

func _summon_meteors():
	emit_signal("summon_meteors", self, 5)

func attacked(damage, knockback_dir = 0):
	health -= damage
	$HitSound.play()
	if health <= 0:
		is_dead = true
		set_collision_mask(0)
		$Cooldown.start() #lol żeby usunąć go dopiero jak spadnie z mapki
	else:
		stun_timer = stun_time
		move_and_slide(Vector2(knockback_dir * 500, 0))

# Atakuj dopiero po skończeniu animacji
func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "Shoot":
		_attack_player()

func _on_Area2D_body_entered(body):
	if body.name == "Protag":
		body.call("attacked", 1)
		$HitSound.play() #żeby gracz nie przegapił tego że dostaje dimejdża

func _on_Cooldown_timeout():
	if is_dead:
		queue_free()

		queue_free()
		emit_signal("emd")
	else:
		emit_signal("start_falling") #jest w cooldownie bo dupa dupa

