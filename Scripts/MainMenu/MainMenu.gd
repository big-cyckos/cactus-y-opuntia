extends "res://Scripts/MainMenu/Menu.gd"
var scena1 = preload("res://Sprites/Scenes/scena1.png")
var scena2 = preload("res://Sprites/Scenes/scena2.png")
var scena3 = preload("res://Sprites/Scenes/scena3.png")
var main = preload("res://Sound/main.ogg")
var intro = preload("res://Sound/intro.wav")

export(String, FILE, "*.tscn") var Level1

func _ready():
	$cutscene/AudioStreamPlayer.set_stream(main)
	$cutscene/AudioStreamPlayer.play()
	$cutscene/Button.visible = false

func _on_StartButton_button_down():
	$cutscene/Sprite.visible = true
	$Label.visible = false
	$VBoxContainer.visible = false
	$cutscene/AudioStreamPlayer.set_stream(intro)
	$cutscene/AudioStreamPlayer.volume_db = -10
	$cutscene/AudioStreamPlayer.play()
	$cutscene/Timer.start()
	print($cutscene/Sprite.texture)
	#get_tree().change_scene(Level1)

func _on_ExitButton_button_down():
	get_tree().quit()

func _on_SettingsButton_button_down():
	get_parent().call("switch_to_menu", "SettingsMenu")

func _on_Timer_timeout():
	if $cutscene/Sprite.texture == scena1:
		$cutscene/Sprite.scale.x = 1.2
		$cutscene/Sprite.scale.y = 1
		$cutscene/Sprite.texture = scena2
		$cutscene/Timer.start()
	elif $cutscene/Sprite.texture == scena2:
		$cutscene/Sprite.texture = scena3
		$cutscene/Timer.start()
	elif $cutscene/Sprite.texture == scena3:
		$cutscene/Button.visible = true


func _on_Button_pressed():
	get_tree().change_scene(Level1)
