# Przełącza między dostępnymi menu dostępnymi menu
extends CanvasLayer

export(String) var DEFAULT_MENU = "MainMenu"

func _ready():
	switch_to_menu(DEFAULT_MENU)

func switch_to_menu(menu_name):
	for menu in get_children():
		if menu.name == menu_name:
			menu.call("show")
		else:
			menu.call("hide")