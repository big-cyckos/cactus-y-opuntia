extends "res://Scripts/MainMenu/Menu.gd"

func show():
	$VBoxContainer/VolumeSlider.value = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Master"))
	visible = true

func _on_VolumeSlider_value_changed(value):
	# kod troche chujowy to wytłumacze
	# skala w tym set_bus_volume_db idzie od 0 do -60, ale poniżej -10 jest już tak cicho że nie warto się pieprzyć,
	# więc jak jest poniżej określonej wartości (teraz -10) po prostu wyciszam bus i chuj
	if value > float($VBoxContainer/VolumeSlider.min_value):
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), false)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), value)
	else:
		AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), true)

func _on_BackButton_button_down():
	get_parent().call("switch_to_menu", "MainMenu")