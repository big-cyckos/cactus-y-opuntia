extends KinematicBody2D

const _up = Vector2(0, -1)
var velocity = Vector2()
var player
var stun_timer = 0
var health
var is_dead
export var max_health = 3
export var stun_time = 0.5
export var attackRange = 250
export var gravity = 10
export var maxGravity = 500

func _ready():
	health = max_health

func init(_player,x = position.x,y = position.y,powerup = false):
	player = _player
	position.x = x
	position.y = y

func _physics_process(delta):
	# grawitacja
	if !is_on_floor():
		velocity.y += gravity
	velocity.y = min(velocity.y, maxGravity)
	
	#stun
	if stun_timer > 0:
		$AnimatedSprite.modulate = Color(1.8, 1.8, 1.8, fmod(stun_timer, 0.25))
		stun_timer -= delta
	else:
		$AnimatedSprite.modulate = Color(1, 1, 1, 1)
	
	# strzelaj jak je gracz
	if player && abs(player.get_position().x - get_position().x) <= attackRange && !$Cooldown.time_left && !is_dead:
		var posDiff = player.get_position().x - get_position().x
		_turn_around()
		
		$AnimatedSprite.play("Attack")
		if $AnimatedSprite.frame == 7: #if on last frame
			_attack_player()
	elif is_on_floor() && $Cooldown.time_left && !is_dead:
		$AnimatedSprite.play("Walk")

	move_and_slide(velocity, _up)
	
	if $Death.frame == 3:
		$AnimatedSprite.visible = false

func _attack_player():
	var tequila = preload("res://tequila.tscn").instance()
	tequila.add_collision_exception_with(self)
	get_parent().add_child(tequila)
	if $AnimatedSprite.flip_h:
		tequila.init(position.x - 10,position.y - 10,player.get_position())
	else:
		tequila.init(position.x + 10,position.y - 10,player.get_position())
	$Cooldown.start()
	
	

func _turn_around():
	$AnimatedSprite.flip_h = player.position.x < position.x

func attacked(damage, knockback_dir = 0):
	health -= damage
	if health <= 0:
		$AudioStreamPlayer2D.set_stream(load("res://SFX/GNU-EXPLODE.wav"))
		$AudioStreamPlayer2D.pitch_scale = 0.9
		$AudioStreamPlayer2D.play()
		is_dead = true
		add_collision_exception_with(player)
		set_collision_mask(33) #wont be affected by notes anymore
		$Death.visible = true
		$Death.play("Death")
		$AnimatedSprite.play("Death")
	else:
		$AudioStreamPlayer2D.play()
		stun_timer = stun_time
		$Cooldown.start()
		$AnimatedSprite.play("Walk")

func _on_Death_animation_finished():
	queue_free()
