extends KinematicBody2D

var velocity = Vector2()
var active = true

func _physics_process(delta):
    if !active:
        return false
    
    move_and_slide(velocity)
    for body in $Area2D.get_overlapping_bodies():
        if body.name == "Protag" and active:
            _explode()
            body.call("attacked", 1)
        elif body.name == "TileMap":
            queue_free()

func _on_start_falling():
    velocity = Vector2(0, 100)

func _explode():
    active = false
    $Sprite.visible = false
    var explosion = preload("res://explosion.tscn").instance()
    get_parent().add_child(explosion)
    $AudioStreamPlayer.play()
    explosion.init(position.x,position.y - 8) #position.y - 8, cuz otherwise explosion takes place in floor and it looks bed

func _on_AudioStreamPlayer_finished():
    queue_free()