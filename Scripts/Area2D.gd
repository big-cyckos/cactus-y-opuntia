extends Area2D
signal dick
signal niszczara


func _on_BossFight_the_end():
	$CollisionShape2D.disabled = false


func _on_Area2D_body_entered(body):
	if body.name == "Protag":
		$CollisionShape2D.disabled = true
		$Sprite.flip_h = false
		$AnimatedSprite.play("death")
		emit_signal("niszczara")


func _on_AnimatedSprite_animation_finished():
	$AnimatedSprite.stop()
	$AnimatedSprite.frame = 8
	$Timer.start()


func _on_Timer_timeout():
	emit_signal("dick")
