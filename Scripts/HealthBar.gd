extends HBoxContainer

func _ready():
	$TextureProgress.value = $TextureProgress.max_value

func _on_Protag_health_change(health, max_health, diff):
	$TextureProgress.value = (float(health) / max_health) * $TextureProgress.max_value

func _on_world_powerup():
	$Sprite.visible = true
	$Timer.start()


func _on_Timer_timeout():
	$Sprite.visible = false
