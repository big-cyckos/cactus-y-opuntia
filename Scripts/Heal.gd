extends StaticBody2D
var once = false

func _on_Heal_body_entered(body):
	if body.name == "Protag" && !once:
		$Particles2D.emitting = true
		body.call("heal")
		body.call("heal")
		once = true
		print("healed")
