extends KinematicBody2D

signal health_change(health, max_health, diff)
signal death
signal init_powerup


const _up = Vector2(0, -1)
var velocity = Vector2()
var health
var invincible = false
export var max_health = 5
export var damage = 1
export var gravity = 10
export var jumpVel = -350
export var speed = 150
export var maxGravity = 500
export var projectileVel = 200

func _ready():
	health = max_health


func _physics_process(delta):
	# animacja nieśmiertelności
	if invincible:
		$AnimatedSprite.modulate = Color(1, 1, 1, fmod($InvincibilityTimer.time_left, 0.25))

	# gravity
	if!(is_on_floor()):
		velocity.y += gravity
	velocity.y = min(velocity.y, maxGravity)
	if position.y > 16:
		visible = false
		position.y = 0 #game crashes without it idk
		emit_signal("death")
	
	# movement
	velocity.x = 0
	if Input.is_action_pressed("ui_left"):
		velocity.x = -speed
		$AnimatedSprite.flip_h = true
	elif Input.is_action_pressed("ui_right"):
		$AnimatedSprite.flip_h = false
		velocity.x = speed
			
	#jump
	if is_on_floor():
		velocity.y = 1
		if Input.is_action_pressed("ui_up"):
			velocity.y = jumpVel
	
	if (Input.is_action_just_released("ui_up") and velocity.y <= 0):
		velocity.y = 0
	
	move_and_slide(velocity, _up) #end of movement code
	#attack
	if Input.is_action_just_pressed("attack") && !($Cooldown.time_left):
		$AudioStreamPlayer2D.set_stream(load("res://SFX/dzwiek" + str(round(rand_range(1,4))) + ".ogg"))
		$AudioStreamPlayer2D.play()
		var bullet = preload("res://note.tscn").instance()
		bullet.add_collision_exception_with(self)
		get_parent().add_child(bullet)
		var bulletDirection
		if $AnimatedSprite.flip_h:
			bulletDirection = -1
		else:
			bulletDirection = 1
		
		bullet.init(position.x,position.y,projectileVel*bulletDirection, damage)
		$Cooldown.start()

	#animation sensation masturbation
	if Input.is_action_pressed("attack"):
		$AnimatedSprite.play("Attack")
	elif velocity.x != 0:
		$AnimatedSprite.play("Walk")
	else:
		$AnimatedSprite.play("Idle")

func attacked(damage):
	if !invincible:
		health -= damage
		emit_signal("health_change", health, max_health, -damage)
		print("cur health: " + str(health) + "/" + str(max_health))

		if health <= 0:
			set_collision_mask(0)
		else:
			# włącz tymczasową nieśmiertelność (jak w mario)
			$InvincibilityTimer.start()
			invincible = true

func heal():
	emit_signal("health_change",health,max_health,max_health-health)
	health = max_health

func _on_InvincibilityTimer_timeout():
	invincible = false
	$AnimatedSprite.modulate = Color(1, 1, 1, 1)

func powerup():
	emit_signal("init_powerup")

func _on_world_powerup():
	$Cooldown.wait_time = 0.01
	$Timer.start()
func _on_Timer_timeout():
	$Cooldown.wait_time = 1