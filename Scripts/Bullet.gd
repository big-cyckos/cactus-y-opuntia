extends KinematicBody2D

var velocity
var stopped_emitting = false

func init(x,y,pos,speed):
	position.x = x
	position.y = y
	look_at(pos)
	velocity = Vector2(speed, 0).rotated(rotation)
	
func _physics_process(delta):
	if !$Particles2D.emitting:
		if stopped_emitting:
			queue_free()
		move_and_slide(velocity)
		for body in $Area2D.get_overlapping_bodies():
			if body.name == "Protag":
				rotation = 0 #if i dont do that, particles will emit upwards when shot to the left
				body.call("attacked", 1)
				$Particles2D.modulate = Color(1,0,0)
				$Particles2D.emitting = true
			elif body.name == "TileMap":
				rotation = 0 #habibi
				$Particles2D.emitting = true
	else:
		$Sprite.visible = false
		stopped_emitting = true
		
	
	
