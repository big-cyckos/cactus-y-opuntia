extends KinematicBody2D

var hit = load("res://SFX/enemy_hit.wav")
var run = load("res://SFX/run.wav")

const _up = Vector2(0, -1)
var velocity = Vector2()
var player
var stun_timer = 0
var health
var is_dead = false
export var max_health = 3
export var stun_time = 0.5
export var attackRange = 250
export var gravity = 10
export var speed = 100
export var maxGravity = 500
var is_init_playing

func _ready():
	health = max_health
	

func init(_player, x = position.x, y = position.y, powerup = false):
	player = _player
	position.x = x
	position.y = y
	$AnimatedSprite.play("Init")
	is_init_playing = ($AnimatedSprite.animation == "Init" && $AnimatedSprite.frame == 4)
	if player.position.x < position.x:
		_turn_around(-1)
	else:
		_turn_around(1)
	

func _physics_process(delta):
	if !is_init_playing:
		is_init_playing = ($AnimatedSprite.animation == "Init" && $AnimatedSprite.frame == 4)
	else:
		is_init_playing = true
	# grawitacja
	if !is_on_floor():
		velocity.y += gravity
	velocity.y = min(velocity.y, maxGravity)
	
	#stun
	if stun_timer > 0:
		$AnimatedSprite.modulate = Color(1.8, 1.8, 1.8, fmod(stun_timer, 0.25))
		stun_timer -= delta
	else:
		$AnimatedSprite.modulate = Color(1, 1, 1, 1)
	
	# attack
	if player && abs(player.get_position().x - get_position().x) <= attackRange && !$Cooldown.time_left && !is_dead && is_init_playing:
		for body in $Area2D.get_overlapping_bodies():
			if body.name == player.name:
				body.call("attacked",1)
		$AnimatedSprite.play("Run")
		if !$AudioStreamPlayer.playing:
			$AudioStreamPlayer.set_stream(run)
			$AudioStreamPlayer.play()
		if $RayCast2D.is_colliding() and !is_on_wall():
			velocity.x = speed
		else:
			_turn_around(null)
			velocity.x = speed
	elif is_init_playing:
		if $AudioStreamPlayer.get_stream() == run:
			$AudioStreamPlayer.playing = false
		


	if is_dead && rotation_degrees < 90:
		speed = 30
		rotation_degrees += 5
	if rotation_degrees >= 90:
		if velocity.x > 0:
			velocity.x -= 1
		else:
			velocity.x += 1
	
	move_and_slide(velocity, _up)
	
	

# dir = 1 to prawo | dir = -1 to lewo | dir = null to dowolne
func _turn_around(dir = null):
	var multiplyAbsBy = -(speed / abs(speed))

	if dir != null:
		multiplyAbsBy = dir
	
	speed = abs(speed) * multiplyAbsBy
	$RayCast2D.cast_to.x = abs($RayCast2D.cast_to.x) * multiplyAbsBy
	$AnimatedSprite.flip_h = (multiplyAbsBy < 0)

func attacked(damage, knockback_dir = 0):
	health -= damage
	if health <= 0:
		is_dead = true
		$AudioStreamPlayer.set_stream(hit)
		$AudioStreamPlayer.play()
		add_collision_exception_with(player)
		set_collision_mask(33) #wont be affected by notes anymore
		$AnimatedSprite.play("Death")
	else:
		stun_timer = stun_time
		$Cooldown.start()
		$AnimatedSprite.play("Idle")

	
	

