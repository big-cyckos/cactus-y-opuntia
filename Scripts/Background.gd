extends Node2D
signal spawn_enemies(type,amount,x,y) #type - 0=gunslinga 1=terrorist 2=runner
signal holokaust
var protag
var were_spawned = false
var were_spawned_obok = false
var were_spawned_terrorist_house = false
var were_spawned_bank = false
var were_spawned_motel = false
var were_spawned_gunslinga = false
var were_spawned_karawan = false
var were_spawned_gunslinga_terrorist = false
var were_spawned_gunslinga_runner = false
var were_spawned_saloon = false


func _on_Area_szopa_body_entered(body):
	if body.name == "Protag" && !were_spawned:
		emit_signal("spawn_enemies",2,1,$Szopa/Szopa.position.x,-8)
		were_spawned = true
		


func _on_Area_obok_szopy_body_entered(body):
	if body.name == "Protag" && !were_spawned_obok:
		emit_signal("spawn_enemies",2,1,-630,-64)
		emit_signal("spawn_enemies",2,1,-550,-64)
		were_spawned_obok = true


func _on_Area_terrorist_house_body_entered(body):
	if body.name == "Protag" && !were_spawned_terrorist_house:
		emit_signal("spawn_enemies",1,1,-246,-75)
		were_spawned_terrorist_house = true


func _on_Area_bank_body_entered(body):
	if body.name == "Protag" && !were_spawned_bank:
		emit_signal("spawn_enemies",1,1,150,-100)
		emit_signal("spawn_enemies",1,1,40,-100)
		were_spawned_bank = true


func _on_Area_motel_body_entered(body):
	if body.name == "Protag" && !were_spawned_motel:
		emit_signal("spawn_enemies",1,1,370,-100)
		emit_signal("spawn_enemies",1,1,480,-100)
		emit_signal("spawn_enemies",2,1,319,-16)
		emit_signal("spawn_enemies",2,1,502,-16)
		were_spawned_motel = true


func _on_Area_gunslinga_body_entered(body):
	if body.name == "Protag" && !were_spawned_gunslinga:
		emit_signal("spawn_enemies",0,1,920,-64)
		were_spawned_gunslinga = true


func _on_Area_karawan_body_entered(body):
	if body.name == "Protag" && !were_spawned_karawan:
		emit_signal("spawn_enemies",0,1,1410,-80)
		emit_signal("spawn_enemies",0,1,1460,-80)
		were_spawned_karawan = true


func _on_Area_gunslinga_terrorist_body_entered(body):
	if body.name == "Protag" && !were_spawned_gunslinga_terrorist:
		emit_signal("spawn_enemies",0,1,1652,-16,1)
		emit_signal("spawn_enemies",0,1,1786,-16)
		emit_signal("spawn_enemies",1,1,1646,-96)
		emit_signal("spawn_enemies",1,1,1782,-96)
		were_spawned_gunslinga_terrorist = true


func _on_Area_runner_gunslinga_body_entered(body):
	if body.name == "Protag" && !were_spawned_gunslinga_runner:
		emit_signal("spawn_enemies",0,1,1896,-80,1)
		emit_signal("spawn_enemies",0,1,1994,-80)
		emit_signal("spawn_enemies",2,1,1896,-16)
		emit_signal("spawn_enemies",2,1,1994,-16)
		were_spawned_gunslinga_runner = true


func _on_Area_saloon_body_entered(body):
	if body.name == "Protag" && !were_spawned_saloon:
		emit_signal("spawn_enemies",2,1,2170,-16)
		emit_signal("spawn_enemies",2,1,2330,-16)
		emit_signal("spawn_enemies",0,1,2176,-34,1)
		emit_signal("spawn_enemies",0,1,2328,-34)
		emit_signal("spawn_enemies",1,1,2176,-92)
		emit_signal("spawn_enemies",1,1,2328,-92)
		were_spawned_saloon = true


func _on_Brothel_body_entered(body):
	if body.name == "Protag":
		emit_signal("holokaust")
		get_tree().change_scene("res://BossFight.tscn")
		pass
