extends Panel

signal retry()

func _ready():
	visible = false

func _on_RetryButton_button_down():
	get_tree().paused = false #żeby po zreloadowaniu gra nie była zapauzowana
	emit_signal("retry")
	get_tree().reload_current_scene()

func _on_Protag_death():
	get_tree().paused = true
	visible = true