extends KinematicBody2D

var powerup
const _up = Vector2(0, -1)
var velocity = Vector2()
var health
var player
var is_dead = false
var stun_timer = 0
export var stun_time = 0.5
export var max_health = 3
export var attackRange = 100
export var gravity = 10
export var speed = 100
export var maxGravity = 500
export var bulletVel = 200
var is_init_playing

func _ready():
	health = max_health

func init(_player, x = position.x, y = position.y,_powerup = false):
	player = _player
	powerup = _powerup
	position.x = x
	position.y = y
	$AnimatedSprite.play("Init")
	is_init_playing = ($AnimatedSprite.animation == "Init" && $AnimatedSprite.frame == 4)
	$AnimatedSprite.flip_h = player.position.x < position.x

func _physics_process(delta): 
	if !is_init_playing:
		is_init_playing = ($AnimatedSprite.animation == "Init" && $AnimatedSprite.frame == 4)
	else:
		is_init_playing = true
	# grawitacja
	if !is_on_floor():
		velocity.y += gravity
	velocity.y = min(velocity.y, maxGravity)
	velocity.x = 0

	#stun
	if stun_timer > 0:
		$AnimatedSprite.modulate = Color(1.8, 1.8, 1.8, fmod(stun_timer, 0.25))
		stun_timer -= delta
	else:
		$AnimatedSprite.modulate = Color(1, 1, 1, 1)

	# strzelaj jak je gracz
	if player && abs(player.get_position().x - get_position().x) <= attackRange && !$Cooldown.time_left && !is_dead && is_init_playing:
		var posDiff = player.get_position().x - get_position().x
		_turn_around(posDiff / abs(posDiff))
		_attack_player()
	elif is_on_floor() && !$Cooldown.time_left && !is_dead && is_init_playing:
		$AnimatedSprite.play("Walk")
		if $RayCast2D.is_colliding() and !is_on_wall():
			velocity.x = speed
		else:
			_turn_around()
			velocity.x = speed
	elif is_init_playing && !is_dead:
		if $AnimatedSprite.frame == 2:
			$AnimatedSprite.stop()
			$AnimatedSprite.frame = 0
	
	move_and_slide(velocity, _up)

func _attack_player():
	if !is_dead:
		var bullet = preload("res://bullet.tscn").instance()
		bullet.add_collision_exception_with(self)
		get_parent().add_child(bullet)
		bullet.init(position.x,position.y,player.get_position(),bulletVel)
		$AudioStreamPlayer.play()
		$AnimatedSprite.play("Attack")
		$Cooldown.start()
	
# dir = 1 to prawo | dir = -1 to lewo | dir = null to dowolne
func _turn_around(dir = null):
	var multiplyAbsBy = -(speed / abs(speed))

	if dir != null:
		multiplyAbsBy = dir
	
	speed = abs(speed) * multiplyAbsBy
	$RayCast2D.cast_to.x = abs($RayCast2D.cast_to.x) * multiplyAbsBy
	$AnimatedSprite.flip_h = (multiplyAbsBy < 0)


func attacked(damage, knockback_dir = 0):
	health -= damage
	if health <= 0:
		$AudioStreamPlayer.set_stream(load("res://SFX/enemy_hit.wav"))
		$AudioStreamPlayer.play()
		is_dead = true
		$AnimatedSprite.play("Death")
		add_collision_exception_with(player)
		set_collision_mask(33) #wont be affected by notes anymore
		if powerup:
			player.call("powerup")
		
	else:
		stun_timer = stun_time
		$Cooldown.start()
		$AnimatedSprite.play("Idle")
		move_and_slide(Vector2(knockback_dir * 500, 0))
	
