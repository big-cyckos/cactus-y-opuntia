extends Panel

signal change_paused(is_paused)
signal restart #for enemies to queue_free themselves

export(String, FILE, "*.tscn") var MenuScene = "res://MainMenu.tscn"
var allow_pausing = true

func _init():
	visible = false
	allow_pausing = true

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel") && (visible || allow_pausing):
		set_game_pause(!visible)

func set_game_pause(is_paused):
	visible = is_paused
	emit_signal("change_paused", is_paused)
	get_tree().paused = is_paused

func _on_ContinueButton_button_down():
	set_game_pause(false)

func _on_ResetButton_button_down():
	set_game_pause(false) #żeby po zreloadowaniu gra nie była zapauzowana
	emit_signal("restart")
	get_tree().reload_current_scene()

func _on_ExitButton_button_down():
	get_tree().paused = false
	get_tree().change_scene(MenuScene)

func _on_Protag_death():
	allow_pausing = false

func _on_GameOver_retry():
	emit_signal("restart")


func _on_Button_pressed():
	get_tree().paused = false
	get_tree().change_scene(MenuScene)
