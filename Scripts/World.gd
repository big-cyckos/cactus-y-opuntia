extends Node
signal powerup()

var gunslinga = preload('res://Enemy.tscn')
var terrorist = preload('res://Terrorist.tscn')
var runner = preload('res://Runner.tscn')

func _ready():
	randomize()
	$AudioStreamPlayer.play()
	var enemies = get_tree().get_nodes_in_group("Enemies")
	for enemy in enemies:
		enemy.init($Protag)


func _on_PauseMenu_restart(): #apparently, if you dont queue_free enemies when reloading scene,
	var enemies = get_tree().get_nodes_in_group("Enemies")#something will shit itself up and make game crash
	for enemy in enemies:
		enemy.queue_free()
	var bullets = get_tree().get_nodes_in_group("Bullets") #and bullets also wont disappear without it
	for bullet in bullets:
		bullet.queue_free()
	


func _on_Background_spawn_enemies(type, amount,x,y,powerup = false):
	for sumthig in range(amount):
		var enemy
		if type==0:
			enemy = gunslinga.instance()
		elif type==1:
			enemy = terrorist.instance()
		elif type==2:
			enemy = runner.instance()
		get_parent().add_child(enemy)
		enemy.init($Protag,x + sumthig * 3,y,powerup)


func _on_Protag_init_powerup():
	emit_signal("powerup")


func _on_Background_holokaust():
	var enemies = get_tree().get_nodes_in_group("Enemies")#something will shit itself up and make game crash
	for enemy in enemies:
		enemy.queue_free()
	var bullets = get_tree().get_nodes_in_group("Bullets") #and bullets also wont disappear without it
	for bullet in bullets:
		bullet.queue_free()
