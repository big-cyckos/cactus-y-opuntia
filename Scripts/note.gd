extends RigidBody2D

var damage

func init(x,y,s,dmg):
	var noteIndex =  round(rand_range(1,5))
	$Note.texture = load('res://Sprites/Bullets/note' + str(noteIndex) + '.png')
	$Note.modulate = Color(rand_range(0,1),rand_range(0,1),rand_range(0,1))
	position.x = x
	position.y = y
	damage = dmg
	linear_velocity.x = s

func _on_Bullet_body_entered(body):
	print(body.name)
	if body.is_in_group("Enemies"):
		print("shited")
		var diff = body.get_position().x - get_position().x
		body.call("attacked", damage,(diff / abs(diff)))
	$CollisionShape2D.disabled = true
	linear_velocity.x = rand_range(-20,20)
	linear_velocity.y = rand_range(-20,20)
	$AnimationPlayer.play('thanos')
		
func _on_AnimationPlayer_animation_finished(anim_name):
	self.queue_free()
