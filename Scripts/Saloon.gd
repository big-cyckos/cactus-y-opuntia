extends StaticBody2D


func init(_protag):
	protag = _protag

func _on_Area2D_body_entered(body):
	if !were_spawned:
		if body == protag:
			emit_signal("spawn_enemies",0,2,110,-30)
			emit_signal("spawn_enemies",2,3,90,-30)
			were_spawned = true
